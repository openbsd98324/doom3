# doom3

rbdoom3bfg 

 scenario game bfg, from the cdrom. 
 needs c:\Games into the .local ... directory.
 it runs slowly on N3050


dhewm3
 it runs very well on N3050


## INTEL AMD64 

1.) debian bookworm

````
  wget -c --no-check-certificate  https://gitlab.com/openbsd98324/linux-debian-bookworm/-/raw/main/linux-debian-bookworm-preinstalled-memstick-kernel-vmlinuz-5.15.0-2-amd64-v1.tar.gz
````

Install the desktop and run apt-get install dhewm3




## VULKAN AMD64 


2.) opensuse Leap 

````
  wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/opensuse-leap-15.3/-/raw/main/rootfs/opensuse-leap-15.3-amd64-x86_64-preinstalled-KDE-notebook.tar.gz" 
````


````
tar xvpfz opensuse-leap-15.3-amd64-x86_64-preinstalled-KDE-notebook.tar.gz
````

Boot with GRUB, and type: 
````
 zypper install dhewm3
````



## Screenshots

![](medias/1642362556-screenshot.png)
![](medias/1642362623-screenshot.png)
![](medias/1642363106-screenshot.png)
![](medias/1642363129-screenshot.png)
![](medias/1642363147-screenshot.png)
![](medias/1642363199-screenshot.png)
![](medias/1642363219-screenshot.png)


![](medias/1642369381-screenshot.png)
![](medias/1642369388-screenshot.png)
![](medias/1642369397-screenshot.png)
![](medias/1642369416-screenshot.png)
![](medias/1642369435-screenshot.png)
![](medias/1642369451-screenshot.png)
